const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: {
    type: String,
    // required: true,
  },
  email: {
    type: String,
    // required: true,
  },
  password: {
      type: String
  },
  address: {
    type: String,
    // required: true
  },
}, { timestamps: true });

const Blog = mongoose.model('users', userSchema);
module.exports = Blog;