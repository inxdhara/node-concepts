var mysql = require('mysql');

var conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'jwtauth'
});

conn.connect(function(error) {
    if(error) {
        throw error;
    } else {
        console.log('Connected');
    }
});

conn.query('select * from users', function(error, result) {
    if (error) throw error;
    console.warn(result);
});