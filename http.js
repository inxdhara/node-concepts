const http = require('http');
const https = require('https')
// var url = require('url');

const options = {
    hostname: 'whatever.com',
    port: 443,
    path: '/todos',
    method: 'GET'
  }
  
  const req = https.request(options, res => {
    console.log(`statusCode: ${res.statusCode}`)
  
    res.on('data', d => {
      process.stdout.write(d)
    })
  })
  
  req.on('error', error => {
    console.error(error)
  })
  
  req.end()


//create a server object:
http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    // res.write(req.url);
    // res.write('Hello World!'); //write a response to the client
    // const headers = res.getHeaders();
    // console.log(headers);
    res.write('Hello World!'); 
    res.end(); //end the response

    // var q = url.parse(req.url, true).query;
    // var txt = q.year + " " + q.month;
    // res.end(txt);
}).listen(8080); //the server object listens on port 8080