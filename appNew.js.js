const express = require('express');
const router = express.Router();
const app = express();
var bodyParser = require('body-parser');
const mongoose = require('mongoose');
const User = require('./users');
var jsonParser = bodyParser.json();

mongoose.connect("mongodb://localhost:27017/jwtauth", {
    useCreateIndex: true, // 
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}).then(() => {
    console.log("connection is successful")
}).catch(() => {
    console.log("No connection")
});

app.get('/users', function (req, res) {
    User.find().then((data) => {
        res.status(200).json(data);
    })
});

app.post('/users', jsonParser, function (req, res) {
    const user = new User({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        email: req.body.email,
        address: req.body.address,
    });
    user.save()
        .then(result => {
            console.warn('result', result);
            res.status(200).json(result);
        })
        .catch(err => {
            console.log(err);
        });
});

app.delete('/users/:id', function (req, res) {
    const id = req.params.id;
    User.findByIdAndDelete(id)
        .then(result => {
            console.warn('result', result);
            res.status(200).json(result);
        })
        .catch(err => {
            console.log(err);
        });
})

app.put('/users/:id', jsonParser, function (req, res) {
    const id = req.params.id;
    User.findByIdAndUpdate(id, req.body, { new: true })
        .then(result => {
            console.warn('result', result);
            res.status(200).json(result);
        })
        .catch(err => {
            console.log(err);
        });
});

app.get('/search/:name', function(req, res) {
    var regex = new RegExp(req.params.name, 'i');
    User.find({name: regex}).then((result) => {
        res.status(200).json(result);
    })
    .catch(err => {
        console.log(err);
    });
});






// const data = new User({
//     _id: new mongoose.Types.ObjectId(),
//     name: 'Dhara',
//     email: 'dhara.inexture@gmail.com',
//     address: 'Ahmedabad'
// });

// data.save()
//     .then(result => {
//       console.warn('result', result);
//     })
//     .catch(err => {
//       console.log(err);
//     });

// User.find({}, function(err, users) {
//     if(err) console.warn(err);
//     console.warn(users);
// })


// const checkUrl = function(req, res, next) {
//     console.log('current route', req.originalUrl);
//     next();
// }

// app.set('view engine', 'ejs');
// // app.use(checkUrl);
// app.get('/', function(req, res) {
//     // res.send('Hello');
//     res.render('home');
// });

// router.get('/login', checkUrl, function(req, res) {
//     // res.send('Hello Login');
//     console.log(req.query);
//     res.render('login');
// });

// router.get('/profile', function(req, res) {
//     // res.send('Hello About');
//     res.render('profile');
// });

// app.use('/', router);
app.listen(4500);